const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('handleGetCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const cars = await carModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const carId = cars[0].id
            await carModel.destroy({ where: { id: carId } })
        })

        it('(valid request body) should return status code 201 and return a new created car', async () => {
            const newCar = {
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            }
            const mReq = { body: newCar }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleCreateCar(mReq, mRes, mNext)


            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {

    })
})
