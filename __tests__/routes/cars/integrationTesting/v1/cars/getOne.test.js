const app = require('../../../../../../app')
const { Car } = require('../../../../../../app/models')
const request = require('supertest')
const { InsufficientAccessError } = require('../../../../../../app/errors')
const { JsonWebTokenError } = require('jsonwebtoken')

describe('Get One Route (GET /v1/cars/:id)', () => {
    const carModel = Car
    
    describe('Successful Operation', () => {
        let token
        const userCredential = {
            email: "brian@binar.co.id",
            password: "123456",
        }
        let car
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    car = await carModel.findByPk(1)
                    done()
                })
            })

        afterAll(() => {

        })
        
        it('(valid request params) should be returning status code 200 and return one single car', (done) => {
            request(app)
                .get('/v1/cars/1')
                .set('Accept', 'application/json')
                .expect("Content-type", /json/)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(200)
                    expect(res.body.id).toEqual(car.id)
                    expect(res.body.name).toEqual(car.name)
                    expect(res.body.size).toEqual(car.size)
                    expect(res.body.image).toEqual(car.image)
                    expect(res.body.isCurrentlyRented).toEqual(car.isCurrentlyRented)

                    done()
                })
                
        })
        
    })
    describe('Error Operation', () => {
        
    })
})