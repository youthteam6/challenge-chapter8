const { Role, User } = require('../../../../../app/models')
const { EmailAlreadyTakenError } = require('../../../../../app/errors');
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");


describe('handleRegister function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const users = await userModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const userId = users[0].id
            await userModel.destroy({ where: { id: userId } })
        })

        it('(valid new email) should return status code 201 and return accessToken of the new registered user', async () => {
            const userCredential = {
                name: "Testing One",
                email: 'testing123@binar.co.id',
                password: "123456"
            }
            
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleRegister(mReq, mRes, mNext)
            
            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeCalledWith(
                expect.objectContaining({
                    accessToken: expect.any(String),
                })
            )
        })
    })

    describe('Error Operation', () => {
        it('(existed email) should return status code 422 and return error EmailAlreadyTakenError', async () => {
            const userCredential = {
                name: 'Brian',
                email: 'brian@binar.co.id',
                password: '123456'
            }
            
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            await authenticationController.handleRegister(mReq, mRes)

            const expectedError = new EmailAlreadyTakenError(mReq.body.email)
            expect(mRes.status).toBeCalledWith(422)
            expect(mRes.json).toBeCalledWith(expectedError)
        })

        it('(empty body) should hit handleError function', async () => {
            const mReq = { body: {  } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()

            await authenticationController.handleRegister(mReq, mRes, mNext)
            
            expect(mNext).toHaveBeenCalled()
        })
    })
})