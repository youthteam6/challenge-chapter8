const { Role, User } = require('../../../../../app/models')
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_SIGNATURE_KEY } = require('../../../../../config/application');


describe('createTokenFromUser function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    describe('Successful Operation', () => {
        let payloadToken
        let user
        let userRole
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payloadToken = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image, 
                role: {
                    id: userRole.id,
                    name: userRole.name,
                }
            }
        })
        
        afterAll(() => {
            
        })
        
        it('(valid user and role) should return jwt token', async () => {
            const expectedToken = jwt.sign(payloadToken, JWT_SIGNATURE_KEY)

            const resultToken = authenticationController.createTokenFromUser(user, userRole)
            expect(resultToken).toEqual(expectedToken)
        })
    })

    describe('Error Operation', () => {
        let payloadToken
        let user
        let userRole


        let wrongUser
        let wrongUserRole
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payloadToken = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image, 
                role: {
                    id: userRole.id,
                    name: userRole.name,
                }
            }

            wrongUser = await userModel.findByPk(5)
            wrongUserRole = await roleModel.findByPk(wrongUser.roleId)
            
        })
        
        afterAll(() => {
            
        })

        it('(invalid user and role) should return invalid jwt token', async () => {
            const expectedToken = jwt.sign(payloadToken, JWT_SIGNATURE_KEY)

            const resultToken = authenticationController.createTokenFromUser(wrongUser, wrongUserRole)
            expect(resultToken).not.toEqual(expectedToken)
        })

        it('(no user and no role) should return error of specific type', async () => {
            const expectedError = new TypeError("Cannot read properties of null (reading 'id')")
            expect(() => authenticationController.createTokenFromUser(null, null)).toThrow(expectedError)
        })
    })
})

