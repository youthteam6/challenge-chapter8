const ApplicationController = require('../../../../../app/controllers/ApplicationController')

describe('getOffsetFromRequest function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request query) should return an offset', async () => {
            const query = { page: 2, pageSize: 5 }
            const expectedOffset = (query.page - 1) * query.pageSize
            const resultOffset = applicationController.getOffsetFromRequest({ query })

            expect(expectedOffset).toEqual(resultOffset)
        })
    })

    describe('Error Operation', () => {
        
    })
})