const ApplicationController = require('../../../../../app/controllers/ApplicationController')

describe('buildPaginationObject function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(empty request query) should return an pagination object', async () => {
            const query = { }
            const expectedPagination = {
                page: 1,
                pageCount: Math.ceil(50 / 10),
                pageSize: 10,
                count: 50
            }
            const resultPagination = applicationController.buildPaginationObject({ query }, expectedPagination.count)

            expect(expectedPagination).toEqual(resultPagination)
        })
    })

    describe('Error Operation', () => {
        
    })
})