const ApplicationController = require('../../../../../app/controllers/ApplicationController')
const { EmailAlreadyTakenError } = require('../../../../../app/errors')


describe('handleNotFound function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request) should return status code 500 and return error with message Internal Server Error', async () => {
            const mReq = { }
            const err = new EmailAlreadyTakenError("brian@binar.co.id")
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            applicationController.handleError(err, mReq, mRes, mNext)

            const expectedResult = {
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details
                }
            }
            expect(mRes.status).toBeCalledWith(500)
            expect(mRes.json).toBeCalledWith(expectedResult)
        })
    })

    describe('Error Operation', () => {
        
    })
})