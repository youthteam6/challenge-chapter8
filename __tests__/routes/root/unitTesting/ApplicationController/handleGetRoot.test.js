const ApplicationController = require('../../../../../app/controllers/ApplicationController')

describe('handleGetRoot function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request) should return status code 200 and return status OK', async () => {
            const mReq = {}
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            applicationController.handleGetRoot(mReq, mRes, mNext)

            const expectedResult = {
                status: "OK",
                message: "BCR API is up and running!",
            }
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeCalledWith(expectedResult)
        })
    })

    describe('Error Operation', () => {

    })
})